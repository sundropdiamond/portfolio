setInterval(createSnowflake, 50);

function createSnowflake(){
    const snowflake = document.createElement("i");

    snowflake.classList.add("fas");
    snowflake.classList.add("fa-circle");
    snowflake.classList.add("fa-xs");

    var pos = String(Math.random() * window.innerWidth) + 'px';
    snowflake.style.left = pos;
    snowflake.style.animationDuration = ((Math.random() * 50) + 1) + 's';
    snowflake.style.opacity = Math.random();
    snowflake.style.fontSize = Math.random() * 10 + 5 + "px";

    $("body").append(snowflake);

    setTimeout(() => {
        snowflake.remove();
    }, 100000);
}
